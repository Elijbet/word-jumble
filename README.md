A Word Jumble game app in React.

Purpose: Study how to debug unnecessary rerenders in React using the DevTools Profiler extension.

Description:

- Type out the unjumbled word. The order of guessing must start from the beginning and you can only guess the next character.
- For every character that was guessed correctly in the right position, the block turns green, or else remains as is.
- If the whole sentence is correctly guessed, there should be a next button that shows at the bottom.
- If you click it or hit enter on your keyboard, it would try to fetch the next sentence from the same API, increasing the counter.
- This would start the next guess.
- Upon scoring 10 points you win the game!

Installation:

npm install
npm start
