import { useEffect, useState } from "react";

export function Input(props) {
	const [nextElement, setNextElement] = useState('')
	//if the state changes to next input, focus on that 
	useEffect(() => {
		if(nextElement){
			nextElement.focus()
		}
	}, [nextElement])
	const handleChange = (e) => {
		let updatedGuesses = [...props.guesses];
		//check each input against value in guesses at that index
		//set the value
		//if it mathces, set whether it's the correct value or not
		updatedGuesses[props.name].value = e.target.value.toLowerCase();
		if (
			updatedGuesses[props.name].value ===
			updatedGuesses[props.name].correctValue
		) {
			updatedGuesses[props.name].isCorrect = true;
		}
		props.setGuesses(updatedGuesses);
		//if the another input follows, set the state to the element
		if(e.target.nextSibling){
			setNextElement(e.target.nextSibling)
		}
	};

	let disabled = true;
	let pass = "";
	let value = "";
	if (props.guesses.length > 0) {
		//previous input is the last index
		let prevInput = props.name - 1;
		//conditionally set the disabled prop on the input
		//if the previous input is filled, enable the next input
		if (prevInput === -1 || props.guesses[prevInput].value !== "") {
			disabled = false;
		}
		//if the input clears as correct value, render it green
		if (props.guesses[props.name].isCorrect) {
			pass = "Green";
		}
		value = props.guesses[props.name].value;
	}

	return (
		<input
			type="text"
			maxLength="1"
			className={[pass, "Block"].join(" ")}
			name={props.name}
			disabled={disabled}
			onChange={handleChange}
			value={value}
		/>
	);
}
