import { Input } from "./Input";
//each sentence is an seperate block of inputs
export function InputBlock({ sentence, guesses, setGuesses }) {
  //to keep track of letter count in total index is set outside of the sentence block
  //each letter is it's own input
  let index = 0;
  let blocks = ''
  blocks = sentence.split(" ").map((word, i) => {
    return (
      <div className="Block-Inner-Container" key={i}>
        {[...word].map((letter) => {
          const input = (
            <Input
              key={index}
              name={index}
              guesses={guesses}
              setGuesses={setGuesses}
            />
          );
          index++;
          return input;
        })}
        <div className={["Block", "Space-Block"].join(" ")} />
      </div>
    );
  });
  return blocks;
}
