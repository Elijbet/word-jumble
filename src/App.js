import "./App.css";
import { useEffect, useState } from "react";
// import axios from "axios";
import { InputBlock } from "./components/InputBlock";

const mockAPI = [
    'I like chocolate cake',
    'Just follow the pattern',
    'Skeletons in the closet',
    'That is a big mountain',
    'Is that seat taken',
    'The food smells wonderful',
    'It happens to everyone',
    'The paint looked new',
    'One more to go',
    'Turn that radio off',
    'Put your coat on'
  ]

const App = () => {
  const [score, setScore] = useState(1);
  const [sentence, setSentence] = useState('');
  const [jumble, setJumble] = useState('');
  const [guesses, setGuesses] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      // const result = await axios(
      //   `https://yourAPI/sentences/${score}`
      // );
      // setSentence(result.data.data.sentence.toLowerCase());
      setSentence(mockAPI[score].toLowerCase());
    };
    fetchData();
    //new sentence is fetched with each score change.
  }, [score]);

  useEffect(() => {
    //guess array is an array of letters, where each letter is an object 
    let initialGuesses = [];
    sentence.split(" ").map((word) => {
      //for letter in the word we push an object to the guesses array that contains the following:
      //for each letter there is an input letter and assessment whether it's the correct input
      return [...word].map((letter) =>
        initialGuesses.push({
          isCorrect: false,
          value: "",
          correctValue: letter,
        })
      );
    });
    setGuesses(initialGuesses);
    //when sentence is set, jumble the sentence and set it to state
    setJumble(jumbleWords(sentence))
    //This useEffect runs every time the sentence is set, which happenes when the data is feched with each score change.
  }, [sentence]);
  
  const jumbleWords = (sentence) => {
    let words = sentence.split(" ")
    //randomize letter within each word, but keep the first and last one in place
    let newWords = words.map((el) => {
      el = el.split("");
      let first = el.shift();
      let last = el.pop() || "";
      return (
        first +
        el
          .sort(() => {
            return 0.5 - Math.random();
          })
          .join("") +
        last
      );
    });
    return newWords.join(" ");
  };
  //if every letter in the guesses array has the prop correct set to true set checkCompleted to true
  let checkCompleted = guesses.length > 0 && guesses.every((guess) => guess.isCorrect === true);
  //if it's true, next round starts. One more point added to the score
  //since score is set, the useEffect will run fetching the new sentence
  //guesses are reset to empty array
  const nextRound = () => {
    if (checkCompleted) {
      let justScored = score;
      justScored++;
      setScore(justScored);
    }
  };

  return (
    <div className="App">
      <header className="App-header">
        {score < 10 ? (
          <div className={["Block", "Radius"].join(" ")}>
            {sentence.length > 0 && (
              <div>
                <h1>{jumble}</h1>
                <h4>
                  Guess the sentence! Start typing.
                  <br />
                  <br />
                  The yellow blocks are meant for spaces.
                </h4>
                <h2>Score: {score}</h2>
                <div className="Outer-Block-Container">
                  <InputBlock 
                    sentence={sentence}
                    guesses={guesses}
                    setGuesses={setGuesses}
                  />
                </div>
                {checkCompleted && (
                  <button 
                    className="Next" 
                    onClick={() => nextRound()}
                    onKeyPress={(e) => {return e.key === 'Enter' ? nextRound() : null}}
                >
                    Next
                  </button>
                )}
              </div>
            )}
          </div>
        ) : (
          <div className={["Win", "Radius"].join(" ")}>You win!</div>
        )}
      </header>
    </div>
  );
};

export default App;
